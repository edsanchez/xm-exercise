package main

import (
	"context"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/segmentio/kafka-go"
	"github.com/spf13/viper"
	"gitlab.com/edsanchez/xm-exercise/config"
	"gitlab.com/edsanchez/xm-exercise/controller"
	"gitlab.com/edsanchez/xm-exercise/gateway"
	"gitlab.com/edsanchez/xm-exercise/handler"
	"gitlab.com/edsanchez/xm-exercise/repository"
	"log"
	"net/http"
)

const (
	_mysqlDSN = "%v:%v@tcp(%v:%v)/%v?charset=utf8&parseTime=True&loc=Local"
)

func main() {
	handleRequests()
}

func handleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)
	serviceConfig, err := getServiceConfig()
	if err != nil {
		log.Printf("error reading config file")
		return
	}

	companyRepository, _ := provideCompanyRepository(serviceConfig.DBConfig)
	kafkaGateway, _ := provideKafkaGateway(serviceConfig.KafkaConfig)
	companyController := controller.New(
		companyRepository,
		kafkaGateway,
	)
	companyHandler := handler.NewCompanyHandler(companyController)
	authenticationHandler := handler.NewAuthenticationHandler(serviceConfig.AuthenticationConfig)

	myRouter.HandleFunc("/login", authenticationHandler.GenerateJWT).Methods("POST")

	myRouter.HandleFunc("/companies", authenticationHandler.VerifyJWT(companyHandler.Create)).Methods("POST")
	myRouter.HandleFunc("/companies/{id}", authenticationHandler.VerifyJWT(companyHandler.Delete)).Methods("DELETE")
	myRouter.HandleFunc("/companies/{id}", companyHandler.Get).Methods("GET")
	myRouter.HandleFunc("/companies/{id}", authenticationHandler.VerifyJWT(companyHandler.Patch)).Methods("PATCH")

	log.Println("starting service: service is active")

	log.Fatal(http.ListenAndServe(":3000", myRouter))
}

func provideCompanyRepository(dbConfig config.DBConfig) (repository.CompanyRepository, error) {
	dsn := fmt.Sprintf(_mysqlDSN,
		dbConfig.User,
		dbConfig.Pass,
		dbConfig.Host,
		dbConfig.Port,
		dbConfig.DBName,
	)
	db, err := gorm.Open("mysql", dsn)
	if err != nil {
		log.Printf("error connecting to database, err: %v\n", err)
		return nil, err
	}

	companyRepository := repository.New(db)
	return companyRepository, nil
}

func provideKafkaGateway(kafkaConfig config.KafkaConfig) (gateway.KafkaGateway, error) {
	conn, err := kafka.DialLeader(context.Background(),
		kafkaConfig.Network,
		kafkaConfig.Address,
		kafkaConfig.Topic,
		kafkaConfig.Partition,
	)
	if err != nil {
		log.Printf("error connecting to kafka, err: %+v", err)
		return nil, err
	}

	kafkaGateway := gateway.New(conn)
	return kafkaGateway, nil
}

func getServiceConfig() (*config.ServiceConfig, error) {
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	viper.SetConfigType("yml")

	if err := viper.ReadInConfig(); err != nil {
		log.Printf("Error reading config file, %s", err)
		return nil, err
	}

	authenticationSecret, _ := viper.Get("AUTHENTICATION.SECRET").(string)
	authenticationDefaultUser, _ := viper.Get("AUTHENTICATION.DEFAULT_USER").(string)
	authenticationDefaultPass, _ := viper.Get("AUTHENTICATION.DEFAULT_PASS").(string)
	authenticationConfig := config.AuthenticationConfig{
		Secret:      authenticationSecret,
		DefaultUser: authenticationDefaultUser,
		DefaultPass: authenticationDefaultPass,
	}

	dbUser, _ := viper.Get("DB.USER").(string)
	dbPass, _ := viper.Get("DB.PASS").(string)
	dbHost, _ := viper.Get("DB.HOST").(string)
	dbPort, _ := viper.Get("DB.PORT").(string)
	dbDBName, _ := viper.Get("DB.DB_NAME").(string)
	dbConfig := config.DBConfig{
		User:   dbUser,
		Pass:   dbPass,
		Host:   dbHost,
		Port:   dbPort,
		DBName: dbDBName,
	}

	kafkaNetwork, _ := viper.Get("KAFKA.NETWORK").(string)
	kafkaAddress, _ := viper.Get("KAFKA.ADDRESS").(string)
	kafkaTopic, _ := viper.Get("KAFKA.TOPIC").(string)
	kafkaPartition, _ := viper.Get("KAFKA.PARTITION").(int)
	kafkaConfig := config.KafkaConfig{
		Network:   kafkaNetwork,
		Address:   kafkaAddress,
		Topic:     kafkaTopic,
		Partition: kafkaPartition,
	}

	serviceConfig := config.ServiceConfig{
		AuthenticationConfig: authenticationConfig,
		DBConfig:             dbConfig,
		KafkaConfig:          kafkaConfig,
	}

	return &serviceConfig, nil
}
