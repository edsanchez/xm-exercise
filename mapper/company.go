package mapper

import (
	"gitlab.com/edsanchez/xm-exercise/entity"
	"gitlab.com/edsanchez/xm-exercise/model"
)

// MapCompanyEntityToModel ...
func MapCompanyEntityToModel(c *entity.Company) *model.Company {
	if c == nil {
		return nil
	}

	return &model.Company{
		ID:                c.ID,
		Name:              c.Name,
		Description:       c.Description,
		AmountOfEmployees: c.AmountOfEmployees,
		Registered:        c.Registered,
		Type:              c.Type,
	}
}

// MapCompanyModelToEntity ...
func MapCompanyModelToEntity(c *model.Company) *entity.Company {
	if c == nil {
		return nil
	}

	return &entity.Company{
		ID:                c.ID,
		Name:              c.Name,
		Description:       c.Description,
		AmountOfEmployees: c.AmountOfEmployees,
		Registered:        c.Registered,
		Type:              c.Type,
	}
}
