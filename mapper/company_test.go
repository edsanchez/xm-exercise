package mapper

import (
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/edsanchez/xm-exercise/entity"
	"gitlab.com/edsanchez/xm-exercise/enums"
	"gitlab.com/edsanchez/xm-exercise/model"
	"testing"
)

func TestMapCompanyEntityToModel(t *testing.T) {
	companyID, _ := uuid.NewV4()
	tests := []struct {
		name           string
		input          *entity.Company
		expectedResult *model.Company
	}{
		{
			name: "success",
			input: &entity.Company{
				ID:                companyID,
				Name:              "name",
				Description:       "description",
				AmountOfEmployees: 2,
				Registered:        true,
				Type:              enums.CORPORATIONS,
			},
			expectedResult: &model.Company{
				ID:                companyID,
				Name:              "name",
				Description:       "description",
				AmountOfEmployees: 2,
				Registered:        true,
				Type:              enums.CORPORATIONS,
			},
		},
		{
			name:           "nil company",
			input:          nil,
			expectedResult: nil,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := MapCompanyEntityToModel(test.input)

			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestMapCompanyModelToEntity(t *testing.T) {
	companyID, _ := uuid.NewV4()
	tests := []struct {
		name           string
		input          *model.Company
		expectedResult *entity.Company
	}{
		{
			name: "success",
			input: &model.Company{
				ID:                companyID,
				Name:              "name",
				Description:       "description",
				AmountOfEmployees: 2,
				Registered:        true,
				Type:              enums.CORPORATIONS,
			},
			expectedResult: &entity.Company{
				ID:                companyID,
				Name:              "name",
				Description:       "description",
				AmountOfEmployees: 2,
				Registered:        true,
				Type:              enums.CORPORATIONS,
			},
		},
		{
			name:           "nil company",
			input:          nil,
			expectedResult: nil,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := MapCompanyModelToEntity(test.input)

			assert.Equal(t, test.expectedResult, result)
		})
	}
}
