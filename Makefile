.PHONY: help

help: ## Show command list
	@ grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## Build the docker compose
	docker-compose build --no-cache

updb: ## Start the database service
	docker-compose up db

initdb: ## Set the database for the first time
	docker exec -it db /usr/bin/mysql -u root --execute "CREATE DATABASE xm_exercise"; exit;
	docker exec -it db /usr/bin/mysql -u root --execute "USE xm_exercise; CREATE TABLE companies( id CHAR(36) NOT NULL PRIMARY KEY, amount_of_employees INT NOT NULL, description VARCHAR(3000) NULL, name VARCHAR(15) NOT NULL UNIQUE, registered TINYINT(1) NOT NULL, type VARCHAR(50) NOT NULL)"; exit;
	docker exec -it db /usr/bin/mysql -u root --execute "CREATE USER 'exercise'@'%' IDENTIFIED WITH mysql_native_password BY 'exercise'";
	docker exec -it db /usr/bin/mysql -u root --execute "GRANT ALL PRIVILEGES ON *.* TO 'exercise'@'%'";

up: ## Start all services
	docker-compose up

stop: ## Stop all services
	docker-compose stop
