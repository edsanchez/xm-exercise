package handler

import (
	"encoding/json"
	"github.com/golang-jwt/jwt"
	"gitlab.com/edsanchez/xm-exercise/config"
	"io"
	"net/http"
	"time"
)

// Credentials ...
type Credentials struct {
	Pass string `json:"pass"`
	User string `json:"user"`
}

// JWTClaim ...
type JWTClaim struct {
	User string `json:"user"`
	jwt.StandardClaims
}

// AuthenticationHandler ...
type AuthenticationHandler interface {
	GenerateJWT(w http.ResponseWriter, r *http.Request)
	VerifyJWT(endpointHandler func(writer http.ResponseWriter, request *http.Request)) http.HandlerFunc
}

type authenticationHandler struct {
	secret      string
	defaultUser string
	defaultPass string
}

// NewAuthenticationHandler ...
func NewAuthenticationHandler(
	authenticationConfig config.AuthenticationConfig,
) AuthenticationHandler {
	return &authenticationHandler{
		secret:      authenticationConfig.Secret,
		defaultUser: authenticationConfig.DefaultUser,
		defaultPass: authenticationConfig.DefaultPass,
	}
}

// GenerateJWT ...
func (h *authenticationHandler) GenerateJWT(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := io.ReadAll(r.Body)
	var credentials Credentials
	json.Unmarshal(reqBody, &credentials)

	// validate default user and password
	if !(credentials.User == h.defaultUser && credentials.Pass == h.defaultPass) {
		w.WriteHeader(http.StatusUnauthorized)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode("invalid credentials")
		return
	}

	expirationTime := time.Now().Add(1 * time.Hour)
	claims := &JWTClaim{
		User: credentials.User,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	key := []byte(h.secret)
	tokenString, err := token.SignedString(key)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(err.Error())
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(tokenString)
}

// VerifyJWT ...
func (h *authenticationHandler) VerifyJWT(endpointHandler func(writer http.ResponseWriter, request *http.Request)) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		if request.Header["Token"] == nil {
			writer.WriteHeader(http.StatusUnauthorized)
			writer.Write([]byte("You're Unauthorized due to No token in the header"))
			return
		}

		token, err := jwt.ParseWithClaims(
			request.Header["Token"][0],
			&JWTClaim{},
			func(token *jwt.Token) (interface{}, error) {
				return []byte(h.secret), nil
			},
		)
		if err != nil {
			writer.WriteHeader(http.StatusUnauthorized)
			writer.Write([]byte("You're Unauthorized due to error parsing the JWT"))
			return
		}
		claims, ok := token.Claims.(*JWTClaim)
		if !ok {
			writer.WriteHeader(http.StatusUnauthorized)
			writer.Write([]byte("couldn't parse claims"))
			return
		}
		if claims.ExpiresAt < time.Now().Local().Unix() {
			writer.WriteHeader(http.StatusUnauthorized)
			writer.Write([]byte("token expired"))
			return
		}
		if !token.Valid {
			writer.WriteHeader(http.StatusUnauthorized)
			writer.Write([]byte("You're Unauthorized due to invalid token"))
			return
		}

		endpointHandler(writer, request)
	}
}
