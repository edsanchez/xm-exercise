package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gofrs/uuid"
	"github.com/gorilla/mux"
	"gitlab.com/edsanchez/xm-exercise/controller"
	"gitlab.com/edsanchez/xm-exercise/entity"
	"gitlab.com/edsanchez/xm-exercise/enums"
	"gitlab.com/edsanchez/xm-exercise/requests"
	"io"
	"net/http"
)

// CompanyHandler ...
type CompanyHandler interface {
	Create(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
	Get(w http.ResponseWriter, r *http.Request)
	Patch(w http.ResponseWriter, r *http.Request)
}

type companyHandler struct {
	companyController controller.CompanyController
}

// NewCompanyHandler ...
func NewCompanyHandler(companyController controller.CompanyController) CompanyHandler {
	return &companyHandler{
		companyController: companyController,
	}
}

// Create ...
func (h *companyHandler) Create(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := io.ReadAll(r.Body)
	var company requests.CreateCompanyRequest
	json.Unmarshal(reqBody, &company)

	// validations
	errMsg := ""
	if company.Name == "" {
		errMsg = "missing name"
	} else if len(company.Name) > 15 {
		errMsg = "invalid name"
	} else if len(company.Description) > 3000 {
		errMsg = "invalid description"
	} else if company.AmountOfEmployees == nil {
		errMsg = "missing amount of employees"
	} else if *company.AmountOfEmployees <= 0 { // assuming all companies should have at least one employee
		errMsg = "invalid amount of employees"
	} else if company.Registered == nil {
		errMsg = "missing registered"
	} else if !enums.IsValid(company.Type) {
		errMsg = "invalid company type"
	}

	if errMsg != "" {
		encodeResponse(w, fmt.Sprintf("invalid request: %s", errMsg), http.StatusBadRequest)
		return
	}

	createdCompany, err := h.companyController.Create(context.Background(), &entity.Company{
		Name:              company.Name,
		Description:       company.Description,
		AmountOfEmployees: *company.AmountOfEmployees,
		Registered:        *company.Registered,
		Type:              company.Type,
	})
	if err != nil {
		encodeResponse(w, err, http.StatusInternalServerError)
		return
	}

	encodeResponse(w, createdCompany, http.StatusCreated)
}

// Delete ...
func (h *companyHandler) Delete(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]

	companyID := uuid.FromStringOrNil(id)

	// validations
	errMsg := ""
	if companyID == uuid.Nil {
		errMsg = "invalid id"
	}

	if errMsg != "" {
		encodeResponse(w, fmt.Sprintf("invalid request: %s", errMsg), http.StatusBadRequest)
		return
	}
	err := h.companyController.Delete(context.Background(), companyID)
	if err != nil {
		encodeResponse(w, err, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// Get ...
func (h *companyHandler) Get(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]

	companyID := uuid.FromStringOrNil(id)

	// validations
	errMsg := ""
	if companyID == uuid.Nil {
		errMsg = "invalid id"
	}

	if errMsg != "" {
		encodeResponse(w, fmt.Sprintf("invalid request: %s", errMsg), http.StatusBadRequest)
		return
	}

	company, err := h.companyController.Get(context.Background(), companyID)
	if err != nil {
		encodeResponse(w, err, http.StatusInternalServerError)
		return
	}
	encodeResponse(w, company, http.StatusOK)
}

// Patch ...
func (h *companyHandler) Patch(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := io.ReadAll(r.Body)
	var company requests.CreateCompanyRequest
	json.Unmarshal(reqBody, &company)

	id := mux.Vars(r)["id"]

	companyID := uuid.FromStringOrNil(id)

	// validations
	errMsg := ""
	if companyID == uuid.Nil {
		errMsg = "invalid id"
	} else if company.Name == "" {
		errMsg = "missing name"
	} else if len(company.Name) > 15 {
		errMsg = "invalid name"
	} else if len(company.Description) > 3000 {
		errMsg = "invalid description"
	} else if company.AmountOfEmployees == nil {
		errMsg = "missing amount of employees"
	} else if *company.AmountOfEmployees <= 0 { // assuming all companies should have at least one employee
		errMsg = "invalid amount of employees"
	} else if company.Registered == nil {
		errMsg = "missing registered"
	} else if !enums.IsValid(company.Type) {
		errMsg = "invalid company type"
	}

	if errMsg != "" {
		encodeResponse(w, fmt.Sprintf("invalid request: %s", errMsg), http.StatusBadRequest)
		return
	}

	err := h.companyController.Patch(context.Background(), &entity.Company{
		ID:                companyID,
		Name:              company.Name,
		Description:       company.Description,
		AmountOfEmployees: *company.AmountOfEmployees,
		Registered:        *company.Registered,
		Type:              company.Type,
	})
	if err != nil {
		encodeResponse(w, err, http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

func encodeResponse(w http.ResponseWriter, responseBody interface{}, statusCode int) {
	w.WriteHeader(statusCode)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(responseBody)
}
