package config

// AuthenticationConfig ...
type AuthenticationConfig struct {
	Secret      string `yaml:"SECRET"`
	DefaultUser string `yaml:"DEFAULT_USER"`
	DefaultPass string `yaml:"DEFAULT_PASS"`
}

// DBConfig ...
type DBConfig struct {
	User   string `yaml:"USER"`
	Pass   string `yaml:"PASS"`
	Host   string `yaml:"HOST"`
	Port   string `yaml:"PORT"`
	DBName string `yaml:"DB_NAME"`
}

// KafkaConfig ...
type KafkaConfig struct {
	Network   string `yaml:"NETWORK"`
	Address   string `yaml:"ADDRESS"`
	Topic     string `yaml:"TOPIC"`
	Partition int    `yaml:"PARTITION"`
}

// ServiceConfig ...
type ServiceConfig struct {
	AuthenticationConfig AuthenticationConfig `yaml:"AUTHENTICATION"`
	DBConfig             DBConfig             `yaml:"DB"`
	KafkaConfig          KafkaConfig          `yaml:"KAFKA"`
}
