package model

import (
	"github.com/gofrs/uuid"
)

// Company ...
type Company struct {
	ID                uuid.UUID `gorm:"id"`
	Name              string    `gorm:"name"`
	Description       string    `gorm:"description"`
	AmountOfEmployees int32     `gorm:"amount_of_employees"`
	Registered        bool      `gorm:"registered"`
	Type              string    `gorm:"type"`
}
