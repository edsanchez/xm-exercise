package repository

import (
	"context"
	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm"
	"gitlab.com/edsanchez/xm-exercise/model"
	"log"
)

// CompanyRepository ...
type CompanyRepository interface {
	Create(ctx context.Context, c *model.Company) error
	Delete(ctx context.Context, id uuid.UUID) error
	Get(ctx context.Context, id uuid.UUID) (*model.Company, error)
	Patch(ctx context.Context, c *model.Company) error
}

type companyRepository struct {
	db *gorm.DB
}

// New ...
func New(db *gorm.DB) CompanyRepository {
	return &companyRepository{
		db: db,
	}
}

// Create ...
func (r *companyRepository) Create(ctx context.Context, c *model.Company) error {
	err := r.db.Debug().
		Create(c).
		Error
	if err != nil {
		log.Printf("Error CompanyRepository::Create ctx: %v, error: %v", ctx, err)
		return err
	}

	log.Printf("Info CompanyRepository::Create ctx: %v, company: %v", ctx, c)
	return nil
}

// Delete ...
func (r *companyRepository) Delete(ctx context.Context, id uuid.UUID) error {
	company := &model.Company{ID: id}
	err := r.db.Debug().
		Delete(company).
		Error
	if err != nil {
		log.Printf("Error CompanyRepository::Delete ctx: %v, error: %v", ctx, err)
		return err
	}

	log.Printf("Info CompanyRepository::Delete ctx: %v", ctx)
	return nil
}

// Get ...
func (r *companyRepository) Get(ctx context.Context, id uuid.UUID) (*model.Company, error) {
	result := &model.Company{}

	err := r.db.Debug().
		First(result, "id = ?", id).
		Error
	if err != nil {
		log.Printf("Error CompanyRepository::Get ctx: %v, error: %v", ctx, err)
		return nil, err
	}

	log.Printf("Info CompanyRepository::Get ctx: %v", ctx)
	return result, nil
}

// Patch ...
func (r *companyRepository) Patch(ctx context.Context, c *model.Company) error {
	err := r.db.Debug().
		Save(c).
		Error
	if err != nil {
		log.Printf("Error CompanyRepository::Patch ctx: %v, error: %v", ctx, err)
		return err
	}

	log.Printf("Info CompanyRepository::Patch ctx: %v, company: %v", ctx, c)
	return nil
}
