package repository

import (
	"context"
	"errors"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/edsanchez/xm-exercise/model"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"testing"
)

func getMockDB(t *testing.T) (*gorm.DB, sqlmock.Sqlmock) {
	sdb, mockSQL, err := sqlmock.New()
	require.NoError(t, err)

	db, err := gorm.Open("mysql", sdb)
	require.NoError(t, err)
	return db, mockSQL
}

func TestNew(t *testing.T) {
	gormdb, _ := getMockDB(t)
	result := New(gormdb)

	assert.NotNil(t, result)
}

func TestCreate(t *testing.T) {
	companyID, _ := uuid.NewV4()

	tests := []struct {
		name          string
		company       *model.Company
		mockSetup     func(sqlMock sqlmock.Sqlmock)
		expectedError error
	}{
		{
			name: "success",
			company: &model.Company{
				ID:                companyID,
				Name:              "name",
				Description:       "description",
				AmountOfEmployees: 2,
				Registered:        true,
				Type:              "CORPORATIONS",
			},
			mockSetup: func(sqlMock sqlmock.Sqlmock) {

				sqlMock.ExpectBegin()
				sqlMock.ExpectExec("INSERT INTO `companies`").
					WithArgs(companyID, "name", "description", 2, true, "CORPORATIONS").
					WillReturnResult(sqlmock.NewResult(1, 1))
				sqlMock.ExpectCommit()
			},
			expectedError: nil,
		},
		{
			name: "error",
			company: &model.Company{
				ID:                companyID,
				Name:              "name",
				Description:       "description",
				AmountOfEmployees: 2,
				Registered:        true,
				Type:              "CORPORATIONS",
			},
			mockSetup: func(sqlMock sqlmock.Sqlmock) {

				sqlMock.ExpectBegin()
				sqlMock.ExpectExec("INSERT INTO `companies`").
					WithArgs(companyID, "name", "description", 2, true, "CORPORATIONS").
					WillReturnError(errors.New("error"))
				sqlMock.ExpectCommit()
			},
			expectedError: errors.New("error"),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			gormDb, sqlMocks := getMockDB(t)

			repository := New(gormDb)

			test.mockSetup(sqlMocks)

			ctx := context.Background()
			err := repository.Create(ctx, test.company)
			assert.Equal(t, test.expectedError, err)
		})
	}
}

func TestDelete(t *testing.T) {
	companyID, _ := uuid.NewV4()

	tests := []struct {
		name          string
		company       uuid.UUID
		mockSetup     func(sqlMock sqlmock.Sqlmock)
		expectedError error
	}{
		{
			name:    "success",
			company: companyID,
			mockSetup: func(sqlMock sqlmock.Sqlmock) {

				sqlMock.ExpectBegin()
				sqlMock.ExpectExec("DELETE FROM `companies`").
					WithArgs(companyID).
					WillReturnResult(sqlmock.NewResult(1, 1))
				sqlMock.ExpectCommit()
			},
			expectedError: nil,
		},
		{
			name:    "error",
			company: companyID,
			mockSetup: func(sqlMock sqlmock.Sqlmock) {

				sqlMock.ExpectBegin()
				sqlMock.ExpectExec("DELETE FROM `companies`").
					WithArgs(companyID).
					WillReturnError(errors.New("error"))
				sqlMock.ExpectCommit()
			},
			expectedError: errors.New("error"),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			gormDb, sqlMocks := getMockDB(t)

			repository := New(gormDb)

			test.mockSetup(sqlMocks)

			ctx := context.Background()
			err := repository.Delete(ctx, companyID)
			assert.Equal(t, test.expectedError, err)
		})
	}
}

func TestGet(t *testing.T) {
	companyID, _ := uuid.NewV4()

	columns := []string{
		`id`,
		`name`,
		`description`,
		`amount_of_employees`,
		`registered`,
		`type`,
	}
	rows := sqlmock.NewRows(columns)
	rows.AddRow(
		companyID,
		"name",
		"description",
		2,
		true,
		"CORPORATIONS",
	)

	tests := []struct {
		name           string
		companyID      uuid.UUID
		mockSetup      func(sqlMock sqlmock.Sqlmock)
		expectedResult *model.Company
		expectedError  error
	}{
		{
			name:      "success",
			companyID: companyID,
			mockSetup: func(sqlMock sqlmock.Sqlmock) {
				sqlMock.ExpectQuery("SELECT").
					WithArgs(companyID).
					WillReturnRows(rows)
			},
			expectedResult: &model.Company{
				ID:                companyID,
				Name:              "name",
				Description:       "description",
				AmountOfEmployees: 2,
				Registered:        true,
				Type:              "CORPORATIONS",
			},
			expectedError: nil,
		},
		{
			name:      "error",
			companyID: companyID,
			mockSetup: func(sqlMock sqlmock.Sqlmock) {
				sqlMock.ExpectQuery("SELECT").
					WithArgs(companyID).
					WillReturnError(errors.New("error"))
			},
			expectedResult: nil,
			expectedError:  errors.New("error"),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			gormDb, sqlMocks := getMockDB(t)

			repository := New(gormDb)

			test.mockSetup(sqlMocks)

			ctx := context.Background()
			result, err := repository.Get(ctx, companyID)
			assert.Equal(t, test.expectedResult, result)
			assert.Equal(t, test.expectedError, err)
		})
	}
}

func TestPatch(t *testing.T) {
	companyID, _ := uuid.NewV4()

	company := &model.Company{
		ID:                companyID,
		Name:              "name",
		Description:       "description",
		AmountOfEmployees: 2,
		Registered:        true,
		Type:              "CORPORATIONS",
	}

	tests := []struct {
		name          string
		company       uuid.UUID
		mockSetup     func(sqlMock sqlmock.Sqlmock)
		expectedError error
	}{
		{
			name:    "success",
			company: companyID,
			mockSetup: func(sqlMock sqlmock.Sqlmock) {

				sqlMock.ExpectBegin()
				sqlMock.ExpectExec("UPDATE `companies`").
					WithArgs("name", "description", 2, true, "CORPORATIONS", companyID).
					WillReturnResult(sqlmock.NewResult(1, 1))
				sqlMock.ExpectCommit()
			},
			expectedError: nil,
		},
		{
			name:    "error",
			company: companyID,
			mockSetup: func(sqlMock sqlmock.Sqlmock) {

				sqlMock.ExpectBegin()
				sqlMock.ExpectExec("UPDATE `companies`").
					WithArgs("name", "description", 2, true, "CORPORATIONS", companyID).
					WillReturnError(errors.New("error"))
				sqlMock.ExpectCommit()
			},
			expectedError: errors.New("error"),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			gormDb, sqlMocks := getMockDB(t)

			repository := New(gormDb)

			test.mockSetup(sqlMocks)

			ctx := context.Background()
			err := repository.Patch(ctx, company)
			assert.Equal(t, test.expectedError, err)
		})
	}
}
