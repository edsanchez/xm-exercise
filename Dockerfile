FROM golang:1.17-alpine

ENV GO111MODULE=on

WORKDIR /app

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN GOOS=linux go build -o xm-exercise

EXPOSE 3000


CMD [ "./xm-exercise" ]