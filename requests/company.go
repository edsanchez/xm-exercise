package requests

// CreateCompanyRequest ...
type CreateCompanyRequest struct {
	Name              string `json:"name"`
	Description       string `json:"description"`
	AmountOfEmployees *int32 `json:"amountOfEmployees"`
	Registered        *bool  `json:"registered"`
	Type              string `json:"type"`
}
