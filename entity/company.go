package entity

import (
	"github.com/gofrs/uuid"
)

// Company ...
type Company struct {
	ID                uuid.UUID `json:"id"`
	Name              string    `json:"name"`
	Description       string    `json:"description"`
	AmountOfEmployees int32     `json:"amountOfEmployees"`
	Registered        bool      `json:"registered"`
	Type              string    `json:"type"`
}
