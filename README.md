# XM Exercise

This is an exercise for a role in XM - Trading point

## Cloning the repo
This repository is hosted in GitLab, please clone the repo using:
```shell
git clone https://gitlab.com/edsanchez/xm-exercise.git
```

Now go to the newly created `xm-exercise` directory
```shell
cd xm-exercise
```

## Setting up
In order to start the main service, database service should be already configured and database already created.

You can see all make commands available by:
```shell
make help
```

**Please, follow the next steps in order**

### How to start the service

- Build containers using docker-compose
```shell
make build
```

#### ONLY THE FIRST TIME
- Start the database service
```shell
make updb
```

- **In another terminal**, set the database for the first time. **This is only on first time.**
```shell
make initdb
```

#### AFTER THE FIRST TIME
- Start all the services
```shell
make up
```

Now that, wait until you can see the log `xm_exercise-web-1  | starting service: service is active`

### Testing
Inside the `testing` directory you can find the following:
- postman collection
- postman variables

If you are familiar with postman you can import those files, and start testing with the following endpoints:
- GetToken
- CreateCompany
- UpdateCompany
- GetCompany
- DeleteCompany

First step is getting the token, once obtained, you can update the token variable in the postman env variables.
This will be used in all other endpoints