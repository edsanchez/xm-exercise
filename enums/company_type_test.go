package enums

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

// TestIsValid ...
func TestIsValid(t *testing.T) {
	tests := []struct {
		name           string
		companyType    string
		expectedResult bool
	}{
		{
			name:           "success - CORPORATIONS",
			companyType:    "CORPORATIONS",
			expectedResult: true,
		},
		{
			name:           "success - NON_PROFIT",
			companyType:    "NON_PROFIT",
			expectedResult: true,
		},
		{
			name:           "success - COOPERATIVE",
			companyType:    "COOPERATIVE",
			expectedResult: true,
		},
		{
			name:           "success - SOLE_PROPRIETORSHIP",
			companyType:    "SOLE_PROPRIETORSHIP",
			expectedResult: true,
		},
		{
			name:           "error - type not supported",
			companyType:    "another type",
			expectedResult: false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := IsValid(test.companyType)

			assert.Equal(t, test.expectedResult, result)
		})
	}
}
