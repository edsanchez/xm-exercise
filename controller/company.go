package controller

import (
	"context"
	"github.com/gofrs/uuid"
	"gitlab.com/edsanchez/xm-exercise/entity"
	"gitlab.com/edsanchez/xm-exercise/gateway"
	"gitlab.com/edsanchez/xm-exercise/mapper"
	"gitlab.com/edsanchez/xm-exercise/repository"
	"log"
)

// CompanyController ...
type CompanyController interface {
	Create(ctx context.Context, c *entity.Company) (*entity.Company, error)
	Delete(ctx context.Context, id uuid.UUID) error
	Get(ctx context.Context, id uuid.UUID) (*entity.Company, error)
	Patch(ctx context.Context, c *entity.Company) error
}

type companyController struct {
	companyRepository repository.CompanyRepository
	kafkaGateway      gateway.KafkaGateway
}

// New ...
func New(
	companyRepository repository.CompanyRepository,
	kafkaGateway gateway.KafkaGateway,
) CompanyController {
	return &companyController{
		companyRepository: companyRepository,
		kafkaGateway:      kafkaGateway,
	}
}

// Create ...
func (ctrl *companyController) Create(ctx context.Context, c *entity.Company) (*entity.Company, error) {
	newCompanyUUID, _ := uuid.NewV4()
	c.ID = newCompanyUUID
	err := ctrl.companyRepository.Create(ctx, mapper.MapCompanyEntityToModel(c))
	if err != nil {
		log.Printf("Error CompanyController::Create ctx: %v, error: %v", ctx, err)
		return nil, err
	}

	err = ctrl.kafkaGateway.Produce(ctx, "company was created")
	if err != nil {
		log.Printf("Error CompanyController::Create ctx: %v, error: %v", ctx, err)
	}

	log.Printf("Info CompanyController::Create ctx: %v, company: %v", ctx, c)
	return c, nil
}

// Delete ...
func (ctrl *companyController) Delete(ctx context.Context, id uuid.UUID) error {
	err := ctrl.companyRepository.Delete(ctx, id)
	if err != nil {
		log.Printf("Error CompanyController::Delete ctx: %v, error: %v", ctx, err)
		return err
	}

	ctrl.kafkaGateway.Produce(ctx, "company was deleted")
	if err != nil {
		log.Printf("Error CompanyController::Delete ctx: %v, error: %v", ctx, err)
	}

	log.Printf("Info CompanyController::Delete ctx: %v", ctx)
	return nil
}

// Get ...
func (ctrl *companyController) Get(ctx context.Context, id uuid.UUID) (*entity.Company, error) {
	c, err := ctrl.companyRepository.Get(ctx, id)
	if err != nil {
		log.Printf("Error CompanyController::Get ctx: %v, error: %v", ctx, err)
		return nil, err
	}

	ctrl.kafkaGateway.Produce(ctx, "company was queried")
	if err != nil {
		log.Printf("Error CompanyController::Get ctx: %v, error: %v", ctx, err)
	}

	log.Printf("Info CompanyController::Get ctx: %v", ctx)
	return mapper.MapCompanyModelToEntity(c), nil
}

// Patch ...
func (ctrl *companyController) Patch(ctx context.Context, c *entity.Company) error {
	err := ctrl.companyRepository.Patch(ctx, mapper.MapCompanyEntityToModel(c))
	if err != nil {
		log.Printf("Error CompanyController::Patch ctx: %v, error: %v", ctx, err)
		return err
	}

	ctrl.kafkaGateway.Produce(ctx, "company was updated")
	if err != nil {
		log.Printf("Error CompanyController::Patch ctx: %v, error: %v", ctx, err)
	}

	log.Printf("Info CompanyController::Patch ctx: %v", ctx)
	return nil
}
