package controller

import (
	"context"
	"errors"
	"github.com/gofrs/uuid"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/edsanchez/xm-exercise/entity"
	"gitlab.com/edsanchez/xm-exercise/mapper"
	gatewayMock "gitlab.com/edsanchez/xm-exercise/mocks/mock_gateway"
	repositoryMock "gitlab.com/edsanchez/xm-exercise/mocks/mock_repository"
	"testing"
)

type mocks struct {
	companyRepository *repositoryMock.MockCompanyRepository
	kafkaGateway      *gatewayMock.MockKafkaGateway
}

func buildController(t *testing.T, mockCtrl *gomock.Controller) (CompanyController, *mocks) {
	mocks := &mocks{
		companyRepository: repositoryMock.NewMockCompanyRepository(mockCtrl),
		kafkaGateway:      gatewayMock.NewMockKafkaGateway(mockCtrl),
	}

	controller := New(
		mocks.companyRepository,
		mocks.kafkaGateway,
	)

	return controller, mocks
}

func TestCreate(t *testing.T) {
	company := &entity.Company{
		Name:              "name",
		Description:       "description",
		AmountOfEmployees: 2,
		Registered:        true,
		Type:              "CORPORATIONS",
	}

	tests := []struct {
		name           string
		input          func() *entity.Company
		mockSetup      func(ctx context.Context, m *mocks)
		expectedResult func() *entity.Company
		expectedError  error
	}{
		{
			name: "success",
			input: func() *entity.Company {
				newCompany := *company
				return &newCompany
			},
			mockSetup: func(ctx context.Context, m *mocks) {
				m.companyRepository.EXPECT().
					Create(ctx, gomock.Any()).
					Return(nil).
					Times(1)

				m.kafkaGateway.EXPECT().
					Produce(ctx, "company was created").
					Return(nil).
					Times(1)
			},
			expectedResult: func() *entity.Company {
				newCompany := *company
				return &newCompany
			},
			expectedError: nil,
		},
		{
			name: "error on repository",
			input: func() *entity.Company {
				newCompany := *company
				return &newCompany
			},
			mockSetup: func(ctx context.Context, m *mocks) {
				m.companyRepository.EXPECT().
					Create(ctx, gomock.Any()).
					Return(errors.New("error")).
					Times(1)
			},
			expectedResult: func() *entity.Company {
				return nil
			},
			expectedError: errors.New("error"),
		},
		{
			name: "error sending kafka message",
			input: func() *entity.Company {
				newCompany := *company
				return &newCompany
			},
			mockSetup: func(ctx context.Context, m *mocks) {
				m.companyRepository.EXPECT().
					Create(ctx, gomock.Any()).
					Return(nil).
					Times(1)

				m.kafkaGateway.EXPECT().
					Produce(ctx, "company was created").
					Return(errors.New("error")).
					Times(1)
			},
			expectedResult: func() *entity.Company {
				newCompany := *company
				return &newCompany
			},
			expectedError: nil,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()

			ctx := context.Background()
			controller, mocks := buildController(t, mockCtrl)
			test.mockSetup(ctx, mocks)

			result, err := controller.Create(ctx, test.input())

			expectedResult := test.expectedResult()
			if expectedResult != nil && result != nil {
				result.ID = expectedResult.ID
			}

			assert.Equal(t, expectedResult, result)
			assert.Equal(t, test.expectedError, err)
		})
	}
}

func TestDelete(t *testing.T) {
	companyID, _ := uuid.NewV4()

	tests := []struct {
		name          string
		mockSetup     func(ctx context.Context, m *mocks)
		expectedError error
	}{
		{
			name: "success",
			mockSetup: func(ctx context.Context, m *mocks) {
				m.companyRepository.EXPECT().
					Delete(ctx, companyID).
					Return(nil).
					Times(1)
				m.kafkaGateway.EXPECT().
					Produce(ctx, "company was deleted").
					Return(nil).
					Times(1)
			},
			expectedError: nil,
		},
		{
			name: "error on repository",
			mockSetup: func(ctx context.Context, m *mocks) {
				m.companyRepository.EXPECT().
					Delete(ctx, companyID).
					Return(errors.New("error")).
					Times(1)
			},
			expectedError: errors.New("error"),
		},
		{
			name: "success",
			mockSetup: func(ctx context.Context, m *mocks) {
				m.companyRepository.EXPECT().
					Delete(ctx, companyID).
					Return(nil).
					Times(1)
				m.kafkaGateway.EXPECT().
					Produce(ctx, "company was deleted").
					Return(errors.New("error")).
					Times(1)
			},
			expectedError: nil,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()

			ctx := context.Background()

			controller, mocks := buildController(t, mockCtrl)
			test.mockSetup(ctx, mocks)

			err := controller.Delete(ctx, companyID)

			assert.Equal(t, test.expectedError, err)
		})
	}
}

func TestGet(t *testing.T) {
	companyID, _ := uuid.NewV4()

	company := &entity.Company{
		ID:                companyID,
		Name:              "name",
		Description:       "description",
		AmountOfEmployees: 2,
		Registered:        true,
		Type:              "CORPORATIONS",
	}

	modelCompany := mapper.MapCompanyEntityToModel(company)
	tests := []struct {
		name           string
		mockSetup      func(ctx context.Context, m *mocks)
		expectedResult *entity.Company
		expectedError  error
	}{
		{
			name: "success",
			mockSetup: func(ctx context.Context, m *mocks) {
				m.companyRepository.EXPECT().
					Get(ctx, companyID).
					Return(modelCompany, nil).
					Times(1)
				m.kafkaGateway.EXPECT().
					Produce(ctx, "company was queried").
					Return(nil).
					Times(1)
			},
			expectedResult: company,
			expectedError:  nil,
		},
		{
			name: "error on repository",
			mockSetup: func(ctx context.Context, m *mocks) {
				m.companyRepository.EXPECT().
					Get(ctx, companyID).
					Return(nil, errors.New("error")).
					Times(1)
			},
			expectedResult: nil,
			expectedError:  errors.New("error"),
		},
		{
			name: "success",
			mockSetup: func(ctx context.Context, m *mocks) {
				m.companyRepository.EXPECT().
					Get(ctx, companyID).
					Return(modelCompany, nil).
					Times(1)
				m.kafkaGateway.EXPECT().
					Produce(ctx, "company was queried").
					Return(errors.New("error")).
					Times(1)
			},
			expectedResult: company,
			expectedError:  nil,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()

			ctx := context.Background()

			controller, mocks := buildController(t, mockCtrl)
			test.mockSetup(ctx, mocks)

			result, err := controller.Get(ctx, companyID)

			assert.Equal(t, test.expectedResult, result)
			assert.Equal(t, test.expectedError, err)
		})
	}
}

func TestPatch(t *testing.T) {
	companyID, _ := uuid.NewV4()

	company := &entity.Company{
		ID:                companyID,
		Name:              "name",
		Description:       "description",
		AmountOfEmployees: 2,
		Registered:        true,
		Type:              "CORPORATIONS",
	}

	tests := []struct {
		name          string
		input         func() *entity.Company
		mockSetup     func(ctx context.Context, m *mocks)
		expectedError error
	}{
		{
			name: "success",
			input: func() *entity.Company {
				newCompany := *company
				return &newCompany
			},
			mockSetup: func(ctx context.Context, m *mocks) {
				m.companyRepository.EXPECT().
					Patch(ctx, gomock.Any()).
					Return(nil).
					Times(1)
				m.kafkaGateway.EXPECT().
					Produce(ctx, "company was updated").
					Return(nil).
					Times(1)
			},
			expectedError: nil,
		},
		{
			name: "error on repository",
			input: func() *entity.Company {
				newCompany := *company
				return &newCompany
			},
			mockSetup: func(ctx context.Context, m *mocks) {
				m.companyRepository.EXPECT().
					Patch(ctx, gomock.Any()).
					Return(errors.New("error")).
					Times(1)
			},
			expectedError: errors.New("error"),
		},
		{
			name: "success",
			input: func() *entity.Company {
				newCompany := *company
				return &newCompany
			},
			mockSetup: func(ctx context.Context, m *mocks) {
				m.companyRepository.EXPECT().
					Patch(ctx, gomock.Any()).
					Return(nil).
					Times(1)
				m.kafkaGateway.EXPECT().
					Produce(ctx, "company was updated").
					Return(errors.New("error")).
					Times(1)
			},
			expectedError: nil,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()

			ctx := context.Background()

			controller, mocks := buildController(t, mockCtrl)
			test.mockSetup(ctx, mocks)

			err := controller.Patch(ctx, company)

			assert.Equal(t, test.expectedError, err)
		})
	}
}
