// Code generated by MockGen. DO NOT EDIT.
// Source: gitlab.com/edsanchez/xm-exercise/repository (interfaces: CompanyRepository)

// Package mock_repository is a generated GoMock package.
package mock_repository

import (
	context "context"
	reflect "reflect"

	uuid "github.com/gofrs/uuid"
	gomock "github.com/golang/mock/gomock"
	model "gitlab.com/edsanchez/xm-exercise/model"
)

// MockCompanyRepository is a mock of CompanyRepository interface.
type MockCompanyRepository struct {
	ctrl     *gomock.Controller
	recorder *MockCompanyRepositoryMockRecorder
}

// MockCompanyRepositoryMockRecorder is the mock recorder for MockCompanyRepository.
type MockCompanyRepositoryMockRecorder struct {
	mock *MockCompanyRepository
}

// NewMockCompanyRepository creates a new mock instance.
func NewMockCompanyRepository(ctrl *gomock.Controller) *MockCompanyRepository {
	mock := &MockCompanyRepository{ctrl: ctrl}
	mock.recorder = &MockCompanyRepositoryMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockCompanyRepository) EXPECT() *MockCompanyRepositoryMockRecorder {
	return m.recorder
}

// Create mocks base method.
func (m *MockCompanyRepository) Create(arg0 context.Context, arg1 *model.Company) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Create", arg0, arg1)
	ret0, _ := ret[0].(error)
	return ret0
}

// Create indicates an expected call of Create.
func (mr *MockCompanyRepositoryMockRecorder) Create(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Create", reflect.TypeOf((*MockCompanyRepository)(nil).Create), arg0, arg1)
}

// Delete mocks base method.
func (m *MockCompanyRepository) Delete(arg0 context.Context, arg1 uuid.UUID) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Delete", arg0, arg1)
	ret0, _ := ret[0].(error)
	return ret0
}

// Delete indicates an expected call of Delete.
func (mr *MockCompanyRepositoryMockRecorder) Delete(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Delete", reflect.TypeOf((*MockCompanyRepository)(nil).Delete), arg0, arg1)
}

// Get mocks base method.
func (m *MockCompanyRepository) Get(arg0 context.Context, arg1 uuid.UUID) (*model.Company, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Get", arg0, arg1)
	ret0, _ := ret[0].(*model.Company)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Get indicates an expected call of Get.
func (mr *MockCompanyRepositoryMockRecorder) Get(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Get", reflect.TypeOf((*MockCompanyRepository)(nil).Get), arg0, arg1)
}

// Patch mocks base method.
func (m *MockCompanyRepository) Patch(arg0 context.Context, arg1 *model.Company) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Patch", arg0, arg1)
	ret0, _ := ret[0].(error)
	return ret0
}

// Patch indicates an expected call of Patch.
func (mr *MockCompanyRepositoryMockRecorder) Patch(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Patch", reflect.TypeOf((*MockCompanyRepository)(nil).Patch), arg0, arg1)
}