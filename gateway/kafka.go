package gateway

import (
	"context"
	"github.com/segmentio/kafka-go"
	"log"
)

// KafkaGateway ...
type KafkaGateway interface {
	Produce(ctx context.Context, message string) error
}

type kafkaGateway struct {
	conn *kafka.Conn
}

// New ...
func New(conn *kafka.Conn) KafkaGateway {
	return &kafkaGateway{
		conn: conn,
	}
}

// Produce ...
func (c *kafkaGateway) Produce(ctx context.Context, message string) error {
	_, err := c.conn.Write([]byte(message))
	if err != nil {
		log.Printf("Error KafkaGateway::Produce failed to write messages: %v, ctx: %v\n", err, ctx)
		return err
	}
	log.Printf("Info KafkaGateway::Produce success to write messages: %v, ctx: %v\n", message, ctx)
	return nil
}
